#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice
import os

Ice.loadSlice('-I. --all drobots.ice')
Ice.loadSlice('-I. --all drobotsAux.ice')

import drobots
import robotController

import math
import random


class FactoriaI(drobots.ControllerFactory):

	def make(self,robot, tipo, jugador, container, contador, current=None):
		print "Make Factoria"

		if(tipo == "defensor"):
			print "Defensor Factoria"
			servantController = robotController.RobotControllerDefensorI()
			proxyController = current.adapter.addWithUUID(servantController)
			directProxyController = current.adapter.createDirectProxy(proxyController.ice_getIdentity())
			controller = drobots.ControllerDefensorPrx.uncheckedCast(directProxyController)
			clave = tipo+"-"+str(contador)+"-"+jugador

			container.linkController(clave,controller)

			print container.listController()
			print("container " + clave)

			controller.setRobot(robot, jugador, container)

			return controller

		if(tipo == "atacante"):
			print "Atacante Factoria"
			servantController = robotController.RobotControllerAtacanteI()
			proxyController = current.adapter.addWithUUID(servantController)
			directProxyController = current.adapter.createDirectProxy(proxyController.ice_getIdentity())
			controller = drobots.ControllerDefensorPrx.uncheckedCast(directProxyController)
			clave = tipo+"-"+str(contador)+"-"+jugador
			container.linkController(clave,controller)
			
			print container.listController()
			print("container " + clave)

			controller.setRobot(robot, jugador,container)

			return controller

		return controller

class Server(Ice.Application):
	def run(self,argv):
		broker = self.communicator()
		servant = FactoriaI()

		adapter = broker.createObjectAdapter("FactoryAdapter")

		proxy = adapter.add(servant, broker.stringToIdentity("factoria"))

		print("Factoria: " + str(proxy))

		proxyContainer = broker.stringToProxy("container")
		factoriasContainer = drobots.ContainerPrx.checkedCast(proxyContainer)

		factoriasContainer.linkFactorias("Factoria"+"-"+str(os.getpid()), proxy)



		adapter.activate()
		self.shutdownOnInterrupt()
		broker.waitForShutdown()
 
		return 0

if __name__ == '__main__':
	sys.exit(Server().main(sys.argv))	

