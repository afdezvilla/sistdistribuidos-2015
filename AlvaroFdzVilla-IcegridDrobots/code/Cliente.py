#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice

Ice.loadSlice('-I. --all drobots.ice')
Ice.loadSlice('-I. --all drobotsAux.ice')

import drobots

import math
import random


class Cliente(Ice.Application):
    def run(self, argv):
                
        jugador = "H" + str(random.randint(0,9999)) 

        print("Jugador: " + jugador)

        broker = self.communicator()

        proxyContainer = broker.stringToProxy("container")
        container = drobots.ContainerPrx.checkedCast(proxyContainer)
        


        adapter = broker.createObjectAdapter("PlayerAdapter")
        servantPlayer = PlayerI(jugador,container)
        proxyPlayer = adapter.addWithUUID(servantPlayer)
        directProxyPlayer = adapter.createDirectProxy(proxyPlayer.ice_getIdentity())
        player = drobots.PlayerPrx.uncheckedCast(directProxyPlayer)

        proxyGame = broker.propertyToProxy("Cliente")
        game = drobots.GamePrx.checkedCast(proxyGame)


        if not game:
            raise RuntimeError('Invalid proxy')

        if not game:
            raise RuntimeError('GameInProgress')

        if not game:
            raise RuntimeError('InvalidName')

        
        adapter.activate()

        game.login(player, jugador)

        self.shutdownOnInterrupt()
        broker.waitForShutdown()
 
        return 0

class PlayerI(drobots.Player):
    def __init__(self,jugador,container):
        self.jugador = jugador
        self.container = container
        self.contador = 0
        
    def makeController(self, robot, current=None):

        print "Make Controller"

        indice = self.contador
        print ("indice " +str(indice))
        lista = self.container.listFactorias()

        if (robot.ice_ids() == ['::Ice::Object', '::drobots::Defender', '::drobots::RobotBase']):
            print "Defensor"
            print ("Usando Factoria " + str(indice))
            value = lista.values()[indice]
            factoria = drobots.ControllerFactoryPrx.checkedCast(value)
            controller = factoria.make(robot, "defensor", self.jugador, self.container, indice)
            self.contador = indice + 1
            print("ControllerDefensor "+str(controller))    
            return controller
        
        elif (robot.ice_ids() == ['::Ice::Object', '::drobots::Attacker', '::drobots::RobotBase']):
            print "Atacante"
            print ("Usando Factoria" + str(indice))
            value = lista.values()[indice]
            factoria = drobots.ControllerFactoryPrx.checkedCast(value)
            controller = factoria.make(robot, "atacante", self.jugador, self.container, indice)
            self.contador = indice + 1
            print("ControllerAtacante "+str(controller))
            return controller


        return controller
    
    def win(self, current=None):
        print("Has Ganado")

    def lose(self, current=None):
        print("Has Perdido")    
    

sys.exit(Cliente().main(sys.argv))
