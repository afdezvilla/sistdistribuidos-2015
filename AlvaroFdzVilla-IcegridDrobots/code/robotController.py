#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice

Ice.loadSlice('-I. --all drobots.ice')
Ice.loadSlice('-I. --all drobotsAux.ice')

import drobots

import math
import random

class RobotControllerAtacanteI(drobots.ControllerAtacante):
	def __init__(self):
		self.robot = None
		self.robotControllerContainer = None
        self.amigos = dict()
        self.defensores = dict()
        self.atacantes = dict()
        self.location = 0
        self.velocidad = 0
        self.energia = 0
        self.angulo = -1
        self.enemigoCoordenadaX = -1
        self.enemigoCoordenadaY = -1
        self.angulosAmigos = []
	
	def setRobot(self,robot, jugador, robotControllerContainer, current = None):
		self.robot = robot
		self.jugador = jugador
		self.robotControllerContainer = robotControllerContainer
        
		
    def turn(self, current = None):
        print " "
        print "Turno Atacante"       
        print ("Jugador " + self.jugador)

        self.energia = 100
        self.location = self.robot.location()
        print ("Posicion Robot - "+str(self.location.x)+","+str(self.location.y))
        self.energia -= 1


        self.amigos = self.poblarAmigos()
        self.defensores = self.poblarDefensores()
        self.atacantes = self.poblarAtacantes()

        self.angulosAmigos = self.getAngulosAmigos(self.location)
    
        indice = random.randint(0,1)
        self.enemigoCoordenadaX = self.coordenadaEnemigoX(indice)

        self.enemigoCoordenadaY = self.coordenadaEnemigoY(indice)

        if(self.enemigoCoordenadaX > -1 and self.enemigoCoordenadaY > -1):
            print "Obteniendo el angulo enemigo"
            puntoX = self.enemigoCoordenadaX - self.location.x
            puntoY = self.enemigoCoordenadaY - self.location.y
            angulo = int(math.degrees(math.atan2(puntoY,puntoX)))
            if(angulo < 0):
                angulo = angulo + 360
            elif(angulo >= 360):
                angulo = 0
            self.angulo = angulo

        if (self.energia > 50 and self.angulo > -1):
            distancia = random.randint(1,10)*100
            self.shoot(self.angulo, distancia)
            self.energia -= 50
        if (self.energia > 60):
            self.moverRombo(self.location)
            self.energia -= 60

    def moverRombo(self, location, current = None):
        print "Mover Rombo"
        if(self.velocidad == 0):
            self.robot.drive(random.randint(0,360),100)
            self.velocidad = 100
        elif(self.location.x > 900):
            self.robot.drive(225, 100)
            self.velocidad = 100
        elif(self.location.x < 100):
            self.robot.drive(45, 100)
            self.velocidad = 100
        elif(self.location.y > 900):
            self.robot.drive(315, 100)
            self.velocidad = 100
        elif(self.location.y < 100):
            self.robot.drive(135, 100)
            self.velocidad = 100

    def shoot(self, angulo, distancia, current = None):
        print("Disparando: grados - "+str(angulo)+" distancia - "+str(distancia))
        self.robot.cannon(angulo, distancia)
        self.angulo = -1
        self.enemigoCoordenadaX = -1
        self.enemigoCoordenadaY = -1

    def coordenadaEnemigoX(self, indice, current = None):
        print "Obteniendo Coordenada X Enemigo"
        coordenadaX = []
        for controller in self.defensores.values():
            defender = drobots.ControllerDefensorPrx.checkedCast(controller)
            puntoXEnemigo = defender.coordenadaEnemigoX()
            coordenadaX.append(puntoXEnemigo)
        puntoXEnemigo = coordenadaX[indice]  
        print puntoXEnemigo 
        return puntoXEnemigo
        
    def coordenadaEnemigoY(self, indice,  current = None):
        print "Obteniendo Coordenada Y Enemigo"
        coordenadaY = []
        for controller in self.defensores.values():
            defender = drobots.ControllerDefensorPrx.checkedCast(controller)
            puntoYEnemigo = defender.coordenadaEnemigoY()
            coordenadaY.append(puntoYEnemigo)
        puntoYEnemigo = coordenadaY[indice] 
        print puntoYEnemigo
        return puntoYEnemigo
    
    def getAngulosAmigos(self,location):
        print "Obteniendo los angulos Amigos"
        angulosAmigos=[]
        for controller in self.defensores.values():
            defensor = drobots.ControllerDefensorPrx.checkedCast(controller)
            coordenada = defensor.posicionAmiga()
            puntoX = math.fabs(coordenada.x - location.x)
            puntoY = math.fabs(coordenada.y - location.y)
            angulo=int(math.degrees(math.atan2(puntoY,puntoX)))
            if(angulo < 0):
                angulo = angulo + 360
            elif(angulo >= 360):
                angulo = 0
            angulosAmigos.append(angulo)

        for controller in self.atacantes.values():
            atacante = drobots.ControllerAtacantePrx.checkedCast(controller)
            coordenada = atacante.posicionAmiga()
            puntoX = math.fabs(coordenada.x - location.x)
            puntoY = math.fabs(coordenada.y - location.y)
            angulo = int(math.degrees(math.atan2(puntoY,puntoX)))
            if(angulo < 0):
                angulo = angulo + 360
            elif(angulo >= 360):
                angulo = 0
            angulosAmigos.append(angulo)

        return angulosAmigos

    def posicionAmiga(self, current = None):
        print "Obteniendo Posicion Amiga"
        location = self.robot.location()
        return location

    def poblarAmigos(self, current = None):
        amigos = dict()
        lista = self.robotControllerContainer.listController()     
        for i in range(0, len(lista)):
            keys = lista.keys()[i]
            value = lista.values()[i]
            if self.jugador in keys:
                amigos[keys] = value
        return amigos

    def poblarAtacantes(self, current = None):
        atacantes = dict()
        for i in range(0, len(self.amigos)):
            keys = self.amigos.keys()[i]
            value = self.amigos.values()[i]
            if "atacante" in keys:
                atacantes[keys] = value
        return atacantes

    def poblarDefensores(self, current = None):
        defensores = dict()
        for i in range(0, len(self.amigos)):
            keys = self.amigos.keys()[i]
            value = self.amigos.values()[i]
            if "defensor" in keys:
                defensores[keys] = value         
        return defensores

    def robotDestroyed(self, current = None):
		print("Robot Destruido")

class RobotControllerDefensorI(drobots.ControllerDefensor):
	def __init__(self):
		self.robot = None
		self.robotControllerContainer = None
        self.amigos = dict()
        self.defensores = dict()
        self.atacantes = dict()
        self.location = 0
        self.velocidad = 0
        self.energia = 0
        self.amplitud = 20
        self.angulo = -1
        self.angulosAmigos = []
        self.coordenadaXEnemigo = -1
        self.coordenadaYEnemigo = -1

	def setRobot(self,robot, jugador, robotControllerContainer,   current = None):
		self.robot = robot
		self.jugador = jugador
		self.robotControllerContainer = robotControllerContainer
        
    def turn(self, current = None):
        print " "
    	print "Turno Defensor"

        
        print ("Jugador " + self.jugador)
        self.energia = 100
        self.location = self.robot.location()
        print ("Posicion Robot - "+str(self.location.x)+","+str(self.location.y))
        self.energia -= 1

        self.amigos = self.poblarAmigos()
        self.defensores = self.poblarDefensores()
        self.atacantes = self.poblarAtacantes()

        self.angulosAmigos = self.getAngulosAmigos(self.location)

        
        self.angulo = random.randint(0, 359)
        
        if(self.energia > 10):
            self.scan(self.angulo, self.amplitud)
        if(self.energia > 60):
            self.moverRombo(self.location)

    def moverRombo(self, location, current = None):
        print "Mover Rombo"
        if(self.velocidad == 0):
            self.robot.drive(random.randint(0,360),100)
            self.velocidad = 100
        elif(self.location.x > 900):
            self.robot.drive(225, 100)
            self.velocidad = 100
        elif(self.location.x < 100):
            self.robot.drive(45, 100)
            self.velocidad = 100
        elif(self.location.y > 900):
            self.robot.drive(315, 100)
            self.velocidad = 100
        elif(self.location.y < 100):
            self.robot.drive(135, 100)
            self.velocidad = 100

    def scan(self, angulo, amplitud, current = None):
        print ("Escaneando: grados - "+str(angulo)+" amplitud - "+str(amplitud))
        self.coordenadaXEnemigo = -1
        self.coordenadaYEnemigo = -1
        enemigo = self.robot.scan(angulo, amplitud)
        if(enemigo > 0):
            for anguloAmigo in self.angulosAmigos:
                if(angulo < anguloAmigo + 10 and angulo > anguloAmigo - 10):
                    print "Amigo Encontrado"
                    self.angulo = -1
                else:
                    print "Enemigo Encontrado"
                    self.getCoordenadasEnemigo(self.location, angulo)
                
    def getAngulosAmigos(self,location):
        print "Obteniendo los angulos Amigos"
        angulosAmigos=[]
        for controller in self.defensores.values():
            defensor = drobots.ControllerDefensorPrx.checkedCast(controller)
            coordenada = defensor.posicionAmiga()
            puntoX = math.fabs(coordenada.x - location.x)
            puntoY = math.fabs(coordenada.y - location.y)
            angulo=int(math.degrees(math.atan2(puntoY,puntoX)))
            if(angulo < 0):
                angulo = angulo + 360
            elif(angulo >= 360):
                angulo = 0
            angulosAmigos.append(angulo)

        for controller in self.atacantes.values():
            atacante = drobots.ControllerAtacantePrx.checkedCast(controller)
            coordenada = atacante.posicionAmiga()
            puntoX = math.fabs(coordenada.x - location.x)
            puntoY = math.fabs(coordenada.y - location.y)
            angulo = int(math.degrees(math.atan2(puntoY,puntoX)))
            if(angulo < 0):
                angulo = angulo + 360
            elif(angulo >= 360):
                angulo = 0
            angulosAmigos.append(angulo)

        return angulosAmigos

    def getCoordenadasEnemigo(self,location, angulo):
        print "Calculando Coordenadas Enemigo"
        if(angulo > -1):
            coordenadaEnemigoX = int((location.x + math.cos(angulo) * random.randint(1,10)*100)%1000)
            self.coordenadaXEnemigo = coordenadaEnemigoX
            coordenadaEnemigoY = int((location.y + math.sin(angulo) * random.randint(1,10)*100)%1000)
            self.coordenadaYEnemigo = coordenadaEnemigoY
        else:
            self.coordenadaXEnemigo = -1
            self.coordenadaYEnemigo = -1

        print "Coordenada X"
        print self.coordenadaXEnemigo
        print "Coordenada Y"
        print self.coordenadaYEnemigo

    def posicionAmiga(self, current = None):
        print "Obteniendo Posicion Amiga"
        location = self.robot.location()
        return location

    def coordenadaEnemigoX(self, current = None):
        coordenadaX = self.coordenadaXEnemigo
        return coordenadaX

    def coordenadaEnemigoY(self, current = None):
        coordenadaY = self.coordenadaYEnemigo
        return coordenadaY

    def poblarAmigos(self, current = None):
        amigos = dict()
        lista = self.robotControllerContainer.listController()       
        for i in range(0, len(lista)):
            keys = lista.keys()[i]
            value = lista.values()[i]
            if self.jugador in keys:
                amigos[keys] = value
        return amigos

    def poblarAtacantes(self, current = None):
        atacantes = dict()
        for i in range(0, len(self.amigos)):
            keys = self.amigos.keys()[i]
            value = self.amigos.values()[i]
            if "atacante" in keys:
                atacantes[keys] = value
        return atacantes

    def poblarDefensores(self, current = None):
        defensores = dict()
        for i in range(0, len(self.amigos)):
            keys = self.amigos.keys()[i]
            value = self.amigos.values()[i]
            if "defensor" in keys:
                defensores[keys] = value               
        return defensores

    def robotDestroyed(self, current = None):
        print("Robot Destruido")